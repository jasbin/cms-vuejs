<?php

namespace App\Http\Controllers\API;

use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->authorize('isAdmin');
        if(Gate::allows('isAdmin') || Gate::allows('isAuthor')){
            return User::latest()->paginate(10);
        }
        return response()->json(['message'=>'unauthorized access']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //stores user's info
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|string|max:191',
            'email'=> 'required|string|email|max:191|unique:users',
            'password'=> 'required|string|min:5',
            'type'=> 'required|string',
            'bio'=> 'required|string'
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'bio'=> $request->bio,
            'type'=> $request->type,
            'password' => Hash::make($request->password)
        ]);
    }

    //return all the info of current logged in user for profile
    public function profile()
    {
        return auth('api')->user();
    }

    //updates profile info
    public function updateProfile(Request $request)
    {
        $user = auth('api')->user();

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|required|min:6'
        ]);

        $currentPhoto = $user->photo;
        if($request->photo && $request->photo != $currentPhoto){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            // store photo inside storage and it needs to be symlink
            $directory = storage_path('app/public/profiles/');
            Image::make($request->photo)->save($directory.$name);
            // delete old photo
            Storage::delete('public/profiles/'.$currentPhoto);
            // save DB
            $request->merge(['photo' => $name]);
        }

        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }

        $user->update($request->all());
        return response()->json([
            'message'=>'user updated successfully'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');

        $user = User::findOrFail($id);

        $this->validate($request,[
            'name'=> 'required|string|max:191',
            'email'=> 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password'=> 'sometimes|string|min:5',
        ]);

        $user->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $user = User::findOrFail($id);
        if($user->email != 'jasbinkarki@gmail.com'){
            $user->delete();
            return response()->json([
                'status'=>204,
                'message'=>'user has been deleted'
            ]);
        }
        return response()->json([
            'status'=>403,
            'message'=>'forbidden'
        ]);
    }
}
